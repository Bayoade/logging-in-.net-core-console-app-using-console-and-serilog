﻿
using Logging_Factory_Console_Application.Service;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using System;

namespace Logging_Factory_Console_Application
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ////Logging using SeriLog to log inside a file
            //Log.Logger = new LoggerConfiguration()
            //  .WriteTo.File("consoleapp.log")
            //  .CreateLogger();

            //var serviceCollection = new ServiceCollection();
            //ConfigureServices(serviceCollection);

            //var serviceProvider = serviceCollection.BuildServiceProvider();

            ////do the actual work here
            //serviceProvider.GetService<IBarService>().DoSomeRealWork();

            //var logger = serviceProvider.GetService<ILogger<Program>>();

            //logger.LogInformation("Log in Program.cs");

            //Logging in a console View
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            var serviceProvider = serviceCollection.BuildServiceProvider();

            //do the actual work here
            var bar = serviceProvider.GetService<IBarService>();
            bar.DoSomeRealWork();

            Console.ReadLine();

        }

        private static void ConfigureServices(IServiceCollection services)
        {
            ////setup our DI for the SeriLog
            //services
            //    .AddLogging(configure => configure.AddSerilog())
            //    .AddTransient<IFooService, FooService>()
            //    .AddTransient<IBarService, BarService>()
            //    .Configure<LoggerFilterOptions>(options => options.MinLevel = LogLevel.Information)
            //    .BuildServiceProvider();

            //setup our DI for the Console View
            services
                .AddLogging(configure => configure.AddConsole())
                //.AddLogging(configure => configure.AddSerilog())
                .AddTransient<IFooService, FooService>()
                .AddTransient<IBarService, BarService>()
                .Configure<LoggerFilterOptions>(options => options.MinLevel = LogLevel.Information)
                .BuildServiceProvider();
        }
    }
}
