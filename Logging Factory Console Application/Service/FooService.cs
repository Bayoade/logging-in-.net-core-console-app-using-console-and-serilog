﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Logging_Factory_Console_Application.Service
{
    public class FooService : IFooService
    {
        private readonly ILogger<FooService> _logger;
        //public FooService(ILoggerFactory loggerFactory)
        //{
        //    _logger = loggerFactory.CreateLogger<FooService>();
        //}
        public FooService(ILogger<FooService> logger)
        {
            _logger = logger;
        }

        public void DoThing(int number)
        {
            _logger.LogInformation($"Doing the thing {number}");
        }
    }
}
